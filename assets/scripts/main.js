/**
 * Created by denidip on 09.09.16.
 */
$(document).foundation();

var navigation = $('.navigation ul.level-1 > li');
var mobile_navigation = $('.navigation');
var go_find = $('button.find');
var go_menu = $('button#go-menu');
var canceler = ($('body').width() - $('.row').width()) * 0.5 + 20;
//var gallaryup = new Foundation.Reveal($('#gallery'));
console.log(canceler);
$(document).ready(function(){

    var slider = $('.slider');
    var slide_item = $('.slide-item');
    var slide_item_2 = $('.slide-item-2');
    var thumbnails = $('#thumb');
    var gallery_slider = $('#gallery_slider');
    var slider_best = $('.slider-best');
    var partners = $('.partners-slider');

    slider.owlCarousel({
        items: 1,
        loop: true,
        mouseDrag: true,
        autoplay: true,
        autoplayTimeout: 4000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        dots: true,
        dotsClass: 'owl-dots row',
        nav: false
    });
    slider_best.owlCarousel({
        items: 1,
        loop: true,
        mouseDrag: true,
        autoplay: true,
        autoplayTimeout: 4000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        dots: false,
        dotsClass: 'owl-dots row',
        nav: true
    });
    slide_item.owlCarousel({
        nav: true,
        navText: '',
        navContainer: '.custom-nav',
        margin: 20,
        loop: false,
        mouseDrag: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 4000,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
    slide_item_2.owlCarousel({
        nav: true,
        navText: '',
        navContainer: '.custom-nav-2',
        margin: 20,
        loop: false,
        mouseDrag: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1200: {
                items: 3
            }
        }
    });
    partners.owlCarousel({
        nav: true,
        navText: '',
        navContainer: '.partners-nav',
        margin: 30,
        loop: true,
        mouseDrag: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 4000,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            },
            1200: {
                items: 5
            }
        }
    });
    thumbnails.owlCarousel({
        nav: false,
        navText: '',
        margin: 0,
        loop: true,
        mouseDrag: true,
        dots: false,
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 4
            },
            1200: {
                items: 6
            }
        }
    });
    gallery_slider.owlCarousel({
        items: 1,
        loop: true,
        mouseDrag: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        dots: true,
        dotsClass: 'owl-dots row',
        nav: true
    });

    navigation.mouseover(function(){
        navigation.removeClass('current');
        $(this).addClass('current');
        $('.find-panel').removeClass('active');
        go_find.removeClass('active');
    });

    go_find.on('click', function(){
        $(this).toggleClass('active');
        $('.find-panel').toggleClass('active');
        $('#find-field').focus();
        mobile_navigation.removeClass('active');
        go_menu.removeClass('is-active');
    });
    go_menu.on('click', function(){
        $(this).toggleClass('is-active');
        go_find.removeClass('active');
        $('.find-panel').removeClass('active');
        mobile_navigation.toggleClass('active');
        $(".sub").parent('li').children('a').css('pointer-events','none');
    });
    $('.slide .title span').append('<div class="canceler"></div>');
    $('.banner-title .title span').append('<div class="canceler"></div>');
    $('.slide .description span').append('<div class="canceler"></div>');
    $('.banner-title .description span').append('<div class="canceler"></div>');
    $('.canceler').css('width',canceler+'px').css('left',(canceler*-1)-20+'px');
    $( window ).resize(function() {
        $('.canceler').css('width',canceler+'px').css('left',(canceler*-1)-20+'px');
    });
    $('.close-slide-panel').on('click', function(){
        $('.slide-panel').removeClass('active');
    });
    $('.map > .overlay').on('click', function(){
        $('.map').toggleClass('active');
        setTimeout(function () {
            myMap.container.fitToViewport();
        }, 750);
        $('.map .wrapper').toggleClass('hide');
        $('body').toggleClass('hidden');
    });
    $('.gallery-thumb .img').on('click', function(){
        gallaryup.open();
        var key = $(this).attr('data-key') - 1;
        gallery_slider.data('owl.carousel').to(key);
    });
});
$(window).scroll(function() {
    navigation.removeClass('current');
    if ($(window).scrollTop() > 30){
        $('header').addClass('scroll');
    }else{
        $('header').removeClass('scroll');
    }
});
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});