(function( $ ) {
    $.fn.calendaryc = function() {

        var calendar = this;
        var day = this.children('.day');

        $(document).on('ready', init);
        $(window).on('resize', heightDay);

        Date.prototype.getMonthName = function() {
            var monthName = ['Январь','Февраль','Март','Апрель','Май','Июнь',
                'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'];
            return monthName[this.getMonth()];
        };


        function init() {
            weekDay();
            generateTable();
            eventCover();
        }
        function daysInMonth(year, month) {
            days = 33 - new Date(year, month-1, 33).getDate();
            return days;
        }
        function getDay(date) { // получить номер дня недели, от 0(пн) до 6(вс)
            var day = date.getDay();
            if (day == 0) day = 7;
            return day - 1;
        }
        function getEvents(cal) {
            var res = [];
            cal.children('.event').each(function(){
                var event = $(this);
                res.push(event);
                $(this).remove();
            });
            return res;
        }
        function generateTable() {
            calendar.each(function() {
                var mouth = $(this).attr('data-calendar-mouth');
                var year = $(this).attr('data-calendar-year');
                var countDays = daysInMonth(year, mouth);
                var events = getEvents($(this));

                var d = new Date(year, mouth-1);
                for (var i = 0; i < getDay(d); i++) {
                    $(this).append('<a class="day null"></a>');
                }

                for(var i = 0; i < countDays; i++){
                    if( (i+1) != $(events[0]).data('calendar-day')){
                        $(this).append(
                            '<a class="day" data-calendar-day="'+(i+1)+'"></a>'
                        );
                    }
                    else{
                        $(this).append(
                            events[0]
                        );
                        events.shift();
                    }
                }

                var d = new Date(year, mouth);
                if (getDay(d) != 0) {
                    for (var i = getDay(d); i < 7; i++) {
                        $(this).append('<a class="day null"></a>');
                    }
                }

                var day = calendar.children('.day');
                heightDay(day);
            })

        }
        function weekDay() {
            calendar.each(function() {
                var mouth = $(this).attr('data-calendar-mouth');
                var year = $(this).attr('data-calendar-year');
                var isMouth = new Date(mouth);
                var currentMouth = isMouth.getMonthName();
                $(this).prepend(
                    '<div class=\"date\">'+currentMouth+', '+year+'</div>'+
                    '<div class=\"week\">'+
                    '<div>пн</div>'+
                    '<div>вт</div>'+
                    '<div>ср</div>'+
                    '<div>чт</div>'+
                    '<div>пт</div>'+
                    '<div>сб</div>'+
                    '<div>вс</div>'+
                    '</div>'
                );
            });
        }
        function heightDay(day) {
            day.height(day.width());
        }
        function eventCover() {

            var event = calendar.children('.event');
            event.each(function() {
                var img = $(this).attr('data-calendar-event-img');
                var title = $(this).attr('data-calendar-event-title');
                var type = $(this).attr('data-calendar-event-type');

                if (typeof img !== typeof undefined && img !== false) {
                    $(this).css('background-image', 'url('+img+')');
                    $(this).append(
                        '<div class=\"ballun\">'+
                        '<div class=\"img\" style=\"background-image: url('+img+');\"></div>'+
                        '<div class=\"title\">'+title+'</div>'+
                        '<div class=\"'+type+'\"></div>'+
                        '</div>'
                    );
                }
            });
        }
        return this;
    };
})(jQuery);


$('.calendar').calendaryc();








/* console.log(calendar.data('calendar-mouth'));
 data-calendar-event

 function daysInMonth() {
     days = 33 - new Date(2016, 12, 33).getDate();
     return days;
 }
 $(function() {
     calendar.setDays();
 });
 function setDays() {
     var text = $(this).data('calendar-mouth');
     console.log(text);
 }*/